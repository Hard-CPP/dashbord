<!-- Side Navbar -->
<nav class="side-navbar">
  <div class="side-navbar-wrapper">
    <div class="sidenav-header d-flex align-items-center justify-content-center">
      <div class="sidenav-header-inner text-center"><img src="asset/img/<?= isset($_SESSION['auth']) ? $_SESSION['auth']['avatar'] : 'male-user.jpg'; ?>" alt="person" class="img-fluid rounded-circle">
        <h2 class="h5 text-uppercase"><?= isset($_SESSION['auth']) ? $_SESSION['auth']['nom'].' '.$_SESSION['auth']['prenom'] : 'test'; ?></h2><span class="text-uppercase"><?= isset($_SESSION['auth']) && $_SESSION['auth']['role'] == 1 ? "Administrateur" : "Visiteur" ?></span>
      </div>
      <div class="sidenav-header-logo"><a href="index.php" class="brand-small text-center"> <strong>D</strong><strong class="text-primary">B</strong></a></div>
    </div>
    <div class="main-menu">
      <ul id="side-main-menu" class="side-menu list-unstyled">                  
        <li class="active"><a href="index.php"> <i class="icon-home"></i><span>Home</span></a></li>
      </ul>
    </div>
    <div class="admin-menu">
      <ul id="side-admin-menu" class="side-menu list-unstyled">
        <?php if (isset($_SESSION['auth']) && $_SESSION['auth']['role'] == 1) : ?>
        <li> <a href="account.php"><i class="icon-presentation"></i><span>Mon Compte</span></a></li>
          <li> <a href="#pages-nav-list" data-toggle="collapse" aria-expanded="false"><i class="icon-user"></i><span>Utilisateurs</span>
              <div class="arrow pull-right"><i class="fa fa-angle-down"></i></div></a>
            <ul id="pages-nav-list" class="collapse list-unstyled">
              <li> <a href="users.php">Afficher les utilisateurs</a></li>
              <li> <a href="create.php">Créer un utilisateur</a></li>
            </ul>
          </li>
        <?php endif;?>
        <?php if (isset($_SESSION['auth'])) : ?>
          <li> <a href="logout.php"><i class="icon-presentation"></i><span>Déconnexion</span></a></li>
        <?php endif; ?>
      </ul>
    </div>
  </div>
</nav>
<div class="page home-page">
<!-- Top Navbar-->
<header class="header">
  <nav class="navbar">
    <div class="container-fluid">
      <div class="navbar-holder d-flex align-items-center justify-content-between">
        <div class="navbar-header"><a id="toggle-btn" href="#" class="menu-btn"><i class="icon-bars"> </i></a><a href="index.php" class="navbar-brand">
            <div class="brand-text hidden-sm-down"><strong class="text-primary">Dashboard</strong></div>
        </div>
        <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
          <?php if (isset($_SESSION['auth'])) : ?>
            <li class="nav-item"><a href="logout.php" class="nav-link logout">Me déconnecter<i class="fa fa-sign-out"></i></a></li>
          <?php else: ?>
              <li class="nav-item"><a href="login.php" class="nav-link logout">M'identifier<i class="fa fa-sign-out"></i></a></li> 
          <?php endif; ?>
        </ul>
      </div>
    </div>
  </nav>
</header>
<?php if (isset($_SESSION['flash'])) : ?>
  <?php foreach ($_SESSION['flash'] as $type => $msg) : ?>
    <div class="alert alert-<?= $type; ?>">
      <?= $msg; ?>
    </div>
  <?php endforeach; ?>
  <?php unset($_SESSION['flash']); ?>
<?php endif; ?>
