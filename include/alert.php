<?php if (isset($_SESSION['flash'])) : ?>
	<?php foreach ($_SESSION['flash'] as $key => $msg) : ?>
	  <div class="alert alert-<?= $key; ?>">
	    <?= $msg; ?>
	  </div>
	<?php endforeach; ?>
	<?php unset($_SESSION['flash']); ?>
<?php endif; ?>