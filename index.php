<?php
	require_once 'include/db.php';
	require_once 'include/head.php';
	require_once 'include/nav.php';

	if (!isset($_SESSION['auth']))
    	header('Location : login.php');

	debug($_SESSION);
?>
      <!-- body Section-->
      <section class="dashboard-counts section-padding">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xl-2 col-md-4 col-6">
              <div class="wrapper count-title d-flex">
                <div class="icon"><i class="fa fa-user-o"></i></div>
                <div class="name"><strong class="text-uppercase">utilisateurs</strong><span>Depuis l'ouverture</span>
                  <div class="count-number">250</div>
                </div>
              </div>
            </div>
            <div class="col-xl-2 col-md-4 col-6">
              <div class="wrapper count-title d-flex">
                <div class="icon"><i class="fa fa-user-plus" aria-hidden="true"></i></div>
                <div class="name"><strong class="text-uppercase">nouveaux utilisateurs</strong><span>Ces 30 derniers jours</span>
                  <div class="count-number">37</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- main Section-->
      <section class="statistics section-padding section-no-padding-bottom">
        <div class="container-fluid">
          <div class="row d-flex align-items-stretch">
            <div class="col-lg-4">
              <div class="wrapper user-activity">
                <h2 class="h5 display">Activité utilisateurs</h2>
                <div class="number">210</div>
                <h3 class="h4 display">Utilisateurs Actifs</h3>
                <div class="progress">
                  <div role="progressbar" style="width: 84%" aria-valuenow="84" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar bg-primary"></div>
                </div>
                <div class="page-statistics d-flex justify-content-between">
                  <div class="page-visites"><span>Pages Visitées</span><strong>2300</strong></div>
                  <div class="new-visites"><span>Nouveaux visiteur</span><strong>73.4%</strong></div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-4">
              <div class="wrapper recent-activities">
                <div id="recent-activities" class="card-header d-flex justify-content-between align-items-center">
                  <h2 class="h5 display"><a data-toggle="collapse" data-parent="#accordion" href="#activities-box" aria-expanded="true" aria-controls="activities-box">Dernières activités</a></h2>
                  <div class="left-column">
                    <div class="badge badge-primary">2 nouvelles</div>
                    <a data-toggle="collapse" data-parent="#accordion" href="#activities-box" aria-expanded="true" aria-controls="activities-box"><i class="fa fa-angle-down"></i></a>
                  </div>
                </div>
                <div id="activities-box" role="tabpanel" class="collapse show">
                  <ul class="activities list-unstyled">
                    <li>
                      <div class="row">
                        <div class="col-4 date-holder text-right">
                          <div class="icon"><i class="icon-clock"></i></div>
                          <div class="date"> <span>16:00</span><span class="text-info">il y a 4 heures</span></div>
                        </div>
                        <div class="col-8 content"><strong>Loremp</strong>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="row">
                        <div class="col-4 date-holder text-right">
                          <div class="icon"><i class="icon-clock"></i></div>
                          <div class="date"> <span>12:00</span><span class="text-info">il y a 6 heures</span></div>
                        </div>
                        <div class="col-8 content"><strong>Loremp</strong>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-5 col-md-12 flex-lg-last flex-md-first align-self-baseline">
              <div class="wrapper sales-report">
                <h2 class="display h4">Historique du trafique</h2>
                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor amet officiis</p>
                <div class="line-chart">
                  <canvas id="lineCahrt"></canvas>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="updates section-padding">
        <div class="container-fluid">
          <div class="row">
            <!--<div class="col-lg-4 col-md-6">
              <div class="wrapper daily-feeds">
                <div id="daily-feeds" class="card-header d-flex justify-content-between align-items-center">
                  <h2 class="h5 display"><a data-toggle="collapse" data-parent="#accordion" href="#feeds-box" aria-expanded="true" aria-controls="feeds-box">Your daily Feeds </a></h2>
                  <div class="left-column">
                    <div class="badge badge-primary">10 messages</div><a data-toggle="collapse" data-parent="#accordion" href="#feeds-box" aria-expanded="true" aria-controls="feeds-box"><i class="fa fa-angle-down"></i></a>
                  </div>
                </div>
                <div id="feeds-box" role="tabpanel" class="collapse show">
                  <div class="feed-box">
                    <ul class="feed-elements list-unstyled">
                      <li class="clearfix">
                        <div class="feed d-flex justify-content-between">
                          <div class="feed-body d-flex justify-content-between"><a href="#" class="feed-profile"><img src="img/avatar-5.jpg" alt="person" class="img-fluid rounded-circle"></a>
                            <div class="content"><strong>Aria Smith</strong><small>Posted a new blog </small>
                              <div class="full-date"><small>Today 5:60 pm - 12.06.2014</small></div>
                            </div>
                          </div>
                          <div class="date"><small>5min ago</small></div>
                        </div>
                      </li>
                      <li class="clearfix">
                        <div class="feed d-flex justify-content-between">
                          <div class="feed-body d-flex justify-content-between"><a href="#" class="feed-profile"><img src="img/avatar-2.jpg" alt="person" class="img-fluid rounded-circle"></a>
                            <div class="content"><strong>Frank Williams</strong><small>Posted a new blog </small>
                              <div class="full-date"><small>Today 5:60 pm - 12.06.2014</small></div>
                              <div class="CTAs"><a href="#" class="btn btn-xs btn-secondary"><i class="fa fa-thumbs-up"> </i>Like</a><a href="#" class="btn btn-xs btn-secondary"><i class="fa fa-heart"> </i>Love</a></div>
                            </div>
                          </div>
                          <div class="date"><small>5min ago</small></div>
                        </div>
                      </li>
                      <li class="clearfix">
                        <div class="feed d-flex justify-content-between">
                          <div class="feed-body d-flex justify-content-between"><a href="#" class="feed-profile"><img src="img/avatar-3.jpg" alt="person" class="img-fluid rounded-circle"></a>
                            <div class="content"><strong>Ashley Wood</strong><small>Posted a new blog </small>
                              <div class="full-date"><small>Today 5:60 pm - 12.06.2014</small></div>
                            </div>
                          </div>
                          <div class="date"><small>5min ago</small></div>
                        </div>
                      </li>
                      <li class="clearfix">
                        <div class="feed d-flex justify-content-between">
                          <div class="feed-body d-flex justify-content-between"><a href="#" class="feed-profile"><img src="img/avatar-1.jpg" alt="person" class="img-fluid rounded-circle"></a>
                            <div class="content"><strong>Jason Doe</strong><small>Posted a new blog </small>
                              <div class="full-date"><small>Today 5:60 pm - 12.06.2014</small></div>
                            </div>
                          </div>
                          <div class="date"><small>5min ago</small></div>
                        </div>
                        <div class="card"> <small>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</small></div>
                        <div class="CTAs pull-right"><a href="#" class="btn btn-xs btn-secondary"><i class="fa fa-thumbs-up"> </i>Like</a></div>
                      </li>
                      <li class="clearfix">
                        <div class="feed d-flex justify-content-between">
                          <div class="feed-body d-flex justify-content-between"><a href="#" class="feed-profile"><img src="img/avatar-6.jpg" alt="person" class="img-fluid rounded-circle"></a>
                            <div class="content"><strong>Sam Martinez</strong><small>Posted a new blog </small>
                              <div class="full-date"><small>Today 5:60 pm - 12.06.2014</small></div>
                            </div>
                          </div>
                          <div class="date"><small>5min ago</small></div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6">
              <div class="wrapper recent-activities">
                <div id="recent-activities" class="card-header d-flex justify-content-between align-items-center">
                  <h2 class="h5 display"><a data-toggle="collapse" data-parent="#accordion" href="#activities-box" aria-expanded="true" aria-controls="activities-box">Dernières activités</a></h2>
                  <div class="left-column">
                    <div class="badge badge-primary">4 nouvelles</div>
                    <a data-toggle="collapse" data-parent="#accordion" href="#activities-box" aria-expanded="true" aria-controls="activities-box"><i class="fa fa-angle-down"></i></a>
                  </div>
                </div>
                <div id="activities-box" role="tabpanel" class="collapse show">
                  <ul class="activities list-unstyled">
                    <li>
                      <div class="row">
                        <div class="col-4 date-holder text-right">
                          <div class="icon"><i class="icon-clock"></i></div>
                          <div class="date"> <span>6:00 am</span><span class="text-info">6 hours ago</span></div>
                        </div>
                        <div class="col-8 content"><strong>Meeting</strong>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="row">
                        <div class="col-4 date-holder text-right">
                          <div class="icon"><i class="icon-clock"></i></div>
                          <div class="date"> <span>6:00 am</span><span class="text-info">6 hours ago</span></div>
                        </div>
                        <div class="col-8 content"><strong>Meeting</strong>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="row">
                        <div class="col-4 date-holder text-right">
                          <div class="icon"><i class="icon-clock"></i></div>
                          <div class="date"> <span>6:00 am</span><span class="text-info">6 hours ago</span></div>
                        </div>
                        <div class="col-8 content"><strong>Meeting</strong>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="row">
                        <div class="col-4 date-holder text-right">
                          <div class="icon"><i class="icon-clock"></i></div>
                          <div class="date"> <span>6:00 am</span><span class="text-info">6 hours ago</span></div>
                        </div>
                        <div class="col-8 content"><strong>Meeting</strong>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>-->
          </div>
        </div>
      </section>
      <?php require_once 'include/footer.php'; ?>