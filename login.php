<?php
	require_once 'include/db.php';
	require_once 'include/head.php';
	require_once 'include/nav.php';
	
	if(!empty($_POST) && !empty($_POST['user']) && !empty($_POST['password']) && !empty($_POST['token'])) {
		$bdd = mysqlConnect();
	    try
	    {
	    	$req = $bdd->prepare('SELECT * FROM users WHERE (username = ? OR email = ?) AND token_key = ? AND validation_token_date IS NOT NULL');
	    	$req->execute([$_POST['user'], $_POST['user'], $_POST['token']]);
	    	$user = $req->fetch();
	    	if (password_verify($_POST['password'], $user['sha_pass_hash']) && $_POST['token'] == $user['token_key']) {
	    		$_SESSION['auth'] = $user;
	    		$_SESSION['avatar'] = $user['avatar'];
	    		$_SESSION['flash']['success'] = 'Vous êtes connecté.';
	    		header('Location: account.php');
	    		exit();
	    	} else {
	    		$_SESSION['flash']['danger'] = 'Identifiants invalides.';
	    	}
	    }
	    catch (PDOException $error) {
	      die ('Erreur : ' .$error->getMessage());
	    }
	}
	debug($_SESSION);
?>
<section class="login-block">
    <div class="container-fluid">
		<div class="row">
			<div class="col-md-8 text-center" id="loginform">
				<form class="form-horizontal" method="post" style="margin-left:480px;margin-top:100px;">
					<fieldset>
						<div class="icon"><i class="fa fa-unlock-alt" aria-hidden="true"></i><strong class="text-uppercase"> Identification</strong><hr></div>
						<!-- Username-->
						<div class="form-group">
						  	<label class="col-md-18 control-label" for="user"></label>
						  	<div class="col-md-18">
						    	<div class="input-group">
						      		<span class="input-group-addon">Utilisateur</span>
						      		<input id="user" name="user" class="form-control" placeholder="Votre pseudo ou votre email" type="text">
					    		</div>
						  	</div>
						</div>
						<!-- Password-->
						<div class="form-group">
						  	<label class="col-md-18 control-label" for="password"></label>
						  	<div class="col-md-18">
							    <div class="input-group">
							      	<span class="input-group-addon">Password</span>
							      	<input id="password" name="password" class="form-control" placeholder="**********" type="password">
							    </div>
						  	</div>
						</div>
						<!-- Token-->
						<div class="form-group">
						  	<label class="col-md-18 control-label" for="token"></label>
						  	<div class="col-md-18">
							    <div class="input-group">
							      	<span class="input-group-addon">Clé secrète</span>
							      	<input id="token" name="token" class="form-control" placeholder="Votre clé secrète" type="text">
							    </div>
						  	</div>
						</div>
						<!-- Button -->
						<div class="form-group">
						  	<label class="col-md-18 control-label" for="btn_login"></label>
						  	<div class="col-md-18">
						    	<button id="btn_login" name="btn_login" class="btn btn-success">Connexion</button>
						  	</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</section>
<?php require_once 'include/footer.php'; ?>