<?php
	require_once 'include/db.php';
	require_once 'include/head.php';
	require_once 'include/nav.php';

  	$id = (int) $_GET['id'];
  	if(empty($id)) {
  		header('Location: users.php');
  		exit();
  	}

  	$bdd = mysqlConnect();
    try
    {
		$user = $bdd->prepare('SELECT * FROM users WHERE id = ?');
		$user->execute(array($id));

    	if (!empty($_POST))
    	{
	        if(!isset($erreur))
	        {
				$sql = $bdd->prepare('UPDATE users SET nom = ?, prenom = ?, email = ?, username = ?, role = ?, sexe = ? WHERE id = ?');
				$sql->execute([$_POST['nom'],$_POST['prenom'],$_POST['email'],$_POST['user'], isset($_POST['role']) ? 1 : 0, $_POST['sexe'], $id]);

				$_SESSION['auth']['role'] = $_POST['role'];
				echo 'l\'Utilisateur '.$_POST['prenom'].' '.$_POST['nom'].' a été mis à jour avec succès.';
				header('Location: users.php');
				exit();
	        }
	        else
	           echo $erreur;
    	}
    }
    catch (PDOException $error) {
      die ('Erreur : ' .$error->getMessage());
    }
?>
<section class="statistics section-padding section-no-padding-bottom">
  <div class="container-fluid">
  	<?php foreach ($user as $key => $data) : ?>
    <div class="row">
      <div class="col-md-12">
        <form class="form-horizontal" method="post" enctype="multipart/form-data">
          <div class="icon"><i class="fa fa-user-plus" aria-hidden="true"></i><strong class="text-uppercase"> Édition de l'utilisateur <?= $data['nom'];?> <?= $data['prenom'];?></strong> <img style="width:4%;" class="img-fluid rounded-circle" src="asset/img/<?= $data['avatar'];?>"><hr></div>
          <fieldset>
            <!-- Nom-->
            <div class="form-group">
              <div class="col-md-18">
                <div class="input-group">
                  <span class="input-group-addon">Nom</span>
                  <input id="nom" name="nom" class="form-control" value="<?= $data['nom'];?>" required="" type="text">
                </div>
                <p class="help-block">Entrer le nom de l'utilisateur</p>
              </div>
            </div>
            <!-- Prenom-->
            <div class="form-group">
              <div class="col-md-18">
                <div class="input-group">
                  <span class="input-group-addon">Prenom</span>
                  <input id="prenom" name="prenom" class="form-control" value="<?= $data['prenom'];?>" required="" type="text">
                </div>
                <p class="help-block">Entrer le prénom de l'utilisateur</p>
              </div>
            </div>
            <!-- Email -->
            <div class="form-group">
              <div class="col-md-18">
                <div class="input-group">
                  <span class="input-group-addon">@</span>
                  <input id="email" name="email" class="form-control" value="<?= $data['email'];?>" required="" type="email">
                </div>
                <p class="help-block">Entrer une adresse email</p>
              </div>
            </div>
            <!-- Username -->
            <div class="form-group">
              <div class="col-md-18">
                <div class="input-group">
                  <span class="input-group-addon">Utilisateur</span>
                  <input id="user" name="user" class="form-control" value="<?= $data['username'];?>" required="" type="text">
                </div>
                <p class="help-block">Entrer un nom d'utilisateur</p>
              </div>
            </div>
            <!-- Select Role -->
            <label class="col-md-18 control-label" for="role">Permissions</label>
            <div class="onoffswitch">
				<?php if ($data['role'] == 0) : ?>
						<input type="checkbox" name="role" class="onoffswitch-checkbox" id="myonoffswitch">
					<?php else: ?>
						<input type="checkbox" name="role" class="onoffswitch-checkbox" id="myonoffswitch" checked>
				<?php endif; ?>
              <label class="onoffswitch-label" for="myonoffswitch">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
              </label>
            </div>
            <p class="help-block">Sélectionner un rôle pour l'utilisateur</p>
            <!-- Select Sexe -->
            <div class="form-group">
              <label class="col-md-18 control-label" for="sexe">Sexe</label>
              <div class="col-md-18">
                <select id="sexe" name="sexe" class="form-control">
	            	<?php if ($data['sexe'] == 0) : ?>
	              		<option value="0" selected="selected">Homme</option>
	              		<option value="1">Femme</option>
	              	<?php else: ?>
	              		<option value="0">Homme</option>
	              		<option value="1" selected="selected">Femme</option>
	              	<?php endif; ?>
                </select>
              </div>
            </div>
            <!-- Button -->
            <div class="form-group">
              <div class="col-md-18">
                <button id="btn_confirm" name="btn_confirm" class="btn btn-success">Mettre à jour l'utilisateur</button>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
	<?php endforeach; ?>
  </div>
</section>
<?php require_once 'include/footer.php'; ?>