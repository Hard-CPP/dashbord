<?php
  require 'functions.php';
  if (session_status() == PHP_SESSION_NONE){
    session_start();
  }

  if (!isset($_SESSION['auth']))
    header('Location : login.php');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" content="IE=edge">
    <title>Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="asset/img/favicon.ico">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="https://file.myfontastic.com/da58YPMQ7U5HY8Rb6UxkNf/icons.css">
  </head>
  <body>
  