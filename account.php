<?php
  require_once 'include/db.php';
  require_once 'include/head.php';
  require_once 'include/nav.php';

  	$bdd = mysqlConnect();
    try
    {
    	if(!empty($_POST) && !empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['email']) && !empty($_POST['user']) && !empty($_POST['password']) && !empty($_POST['passwordconfirm']) && !empty($_POST['token'])) {
    		$errors = array();

    		if (empty($_POST['username']) || !preg_match('/^[a-zA-Z0-9_]+$/', $_POST['username'])) {
		        $errors['username'] = "Pseudo invalide, seul des caractères alphanumérique sont acceptés.";
	      	} else {
		        $user = $bdd->prepare('SELECT * FROM users WHERE username = ?');
		        $user->execute([$_POST['username']]);
		        $username = $user->fetch();
		        if ($username) {
		          $errors['username'] = "Ce nom d'utilisateur exite déjà.";
		        }
	      	}

      		if(empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
		        $errors['email'] = "l'adresse email n'est pas valide";
	      	} else {
		        $email = $bdd->prepare('SELECT * FROM users WHERE email = ?');
		        $email->execute([$_POST['email']]);
		        $mail = $email->fetch();
		        if ($mail) {
		          $errors['username'] = "Cette adresse email exite déjà.";
		        }
			}

			if (empty($_POST['password']) || $_POST['password'] != $_POST['passwordconfirm']) {
				$errors['password'] = "Vous devez entrez un mot de passe valide";
			}

	        if(!isset($erreur)) {
	        	$avatar = strtr(basename($_FILES['avatar']['name']), 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
				$avatar = preg_replace('/([^.a-z0-9]+)/i', '-', $avatar);
				if(!move_uploaded_file($_FILES['avatar']['tmp_name'], 'asset/img/' . $avatar)) {
					$errors['upload'] = 'Echec de l\'upload !';
					header('Location: account.php');
					exit();
				} else {
					$password = password_hash($_POST['password'], PASSWORD_DEFAULT);
					$req = $bdd->prepare('UPDATE users SET nom = ?, prenom = ?, email = ?, username = ?, sha_pass_hash = ?, avatar = ? WHERE id = ?');
					$req->execute([$_POST['nom'], $_POST['prenom'], $_POST['email'], $_POST['user'], $password, $avatar, $_SESSION['auth']['id']]);

					$_SESSION['flash']['success'] = 'Vos données ont été mise à jour.';
					header('Location: account.php');
					exit();
				}
	        }
    	}
    }
    catch (PDOException $error) {
      die ('Erreur : ' .$error->getMessage());
    }
?>
<!-- Form -->
<section class="statistics section-padding section-no-padding-bottom">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <form class="form-horizontal" method="post" enctype="multipart/form-data">
          <div class="icon"><img style="width:4%;" class="img-fluid rounded-circle" src="asset/img/<?=$_SESSION['avatar']; ?>"><strong class="text-uppercase"> Mon compte</strong><hr></div>
          <fieldset>
            <!-- Nom-->
            <div class="form-group">
              <div class="col-md-18">
                <div class="input-group">
                  <span class="input-group-addon">Nom</span>
                  <input id="nom" name="nom" class="form-control" value="<?= $_SESSION['auth']['nom']; ?>" required="" type="text">
                </div>
                <p class="help-block">Entrer votre nom</p>
              </div>
            </div>
            <!-- Prenom-->
            <div class="form-group">
              <div class="col-md-18">
                <div class="input-group">
                  <span class="input-group-addon">Prenom</span>
                  <input id="prenom" name="prenom" class="form-control" value="<?= $_SESSION['auth']['prenom']; ?>" required="" type="text">
                </div>
                <p class="help-block">Entrer votre prénom</p>
              </div>
            </div>
            <!-- Email -->
            <div class="form-group">
              <div class="col-md-18">
                <div class="input-group">
                  <span class="input-group-addon">@</span>
                  <input id="email" name="email" class="form-control" value="<?= $_SESSION['auth']['email']; ?>" required="" type="email">
                </div>
                <p class="help-block">Entrer une adresse email</p>
              </div>
            </div>
            <!-- Username -->
            <div class="form-group">
              <div class="col-md-18">
                <div class="input-group">
                  <span class="input-group-addon">Utilisateur</span>
                  <input id="user" name="user" class="form-control" value="<?= $_SESSION['auth']['username']; ?>" required="" type="text">
                </div>
                <p class="help-block">Entrer un nom d'utilisateur</p>
              </div>
            </div>
            <!-- Password -->
            <div class="form-group">
              <div class="col-md-18">
                <div class="input-group">
                  <span class="input-group-addon">Password</span>
                  <input id="password" name="password" class="form-control" placeholder="********" required="" type="password">
                </div>
                <p class="help-block">Entrer un mot de passe</p>
              </div>
            </div>
            <!-- Confirm Password -->
            <div class="form-group">
              <div class="col-md-18">
                <div class="input-group">
                  <span class="input-group-addon">Confirmation</span>
                  <input id="passwordconfirm" name="passwordconfirm" class="form-control" placeholder="********" required="" type="password">
                </div>
                <p class="help-block">Confirmer le mot de passe</p>
              </div>
            </div>
            <!-- Authentification Key -->
            <div class="form-group">
              <div class="col-md-18">
                <div class="input-group">
                  <span class="input-group-addon">Key d'authenfication</span>
                  <input id="authkey" name="authkey" disabled="true" class="form-control" value="<?= $_SESSION['auth']['token_key']; ?>" required="" type="text">
                </div>
                <p class="help-block">Confirmer le mot de passe</p>
              </div>
            </div>
            <!-- Avatar --> 
            <div class="form-group">
              <label class="col-md-18 control-label" for="avatar">Avatar</label>
              <div class="col-md-18">
                <input type="hidden" name="MAX_FILE_SIZE" value="2000000000">
                <input name="avatar" class="input-file" type="file">
              </div>
            </div>
            <!-- Button -->
            <div class="form-group">
              <div class="col-md-18">
                <button id="btn_confirm" name="btn_confirm" class="btn btn-success">Créer l'utilisateur</button>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</section>
<?php require_once 'include/footer.php'; ?>