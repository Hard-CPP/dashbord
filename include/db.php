<?php

function mysqlConnect()
{
	try {
		$db = new PDO('mysql:host=localhost;dbname=exercice;charset=utf8','root','', [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
	}
	catch (PDOException $error) {
		die ('Erreur : ' .$error->getMessage());
	}

	return $db;
}

?>