<?php
	function debug($var){
		echo '<pre>' . print_r($var, true) . '</pre>';
	}

	function str_rand_token($length) {
		$alpha = "0123456789azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN";
		return substr(str_shuffle(str_repeat($alpha, $length)), 0, $length);
	}