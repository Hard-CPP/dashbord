<?php
	session_start();
	unset($_SESSION['auth']);
	unset($_SESSION['auth']['role']);
	unset($_SESSION['avatar']);
	$_SESSION['flash']['success'] = 'Vous avez été déconnecté.';
	header('Location: login.php');
	exit();