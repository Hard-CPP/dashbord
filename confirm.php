<?php
	require_once 'include/db.php';

	$id = $_GET['id'];
	$token = $_GET['token'];

	var_dump($_GET);

	$bdd = mysqlConnect();
    try
    {
    	$req = $bdd->prepare('SELECT username, validation_token FROM users WHERE id = ?');
      	$req->execute(array($id));

      	$user = $req->fetch();

      	session_start();

      	if ($user && $user['validation_token'] == $token) {
      		$req = $bdd->prepare('UPDATE users SET validation_token = NULL, validation_token_date = NOW() WHERE id = ?')->execute(array($id));
      		$_SESSION['flash']['success'] = 'Votre compte id '.$id->username.'a été validé.';
      		$_SESSION['auth'] = $user;
      		header('Location: account.php');
      		exit();
      	}
      	else {
      		$_SESSION['flash']['danger'] = "Ce token de validation n'est plus valide.";
			header('Location: login.php');
			exit();
      	}
    }
    catch (PDOException $error) {
      die ('Erreur : ' .$error->getMessage());
    }