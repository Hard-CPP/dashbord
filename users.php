<?php
  require_once 'include/db.php';
  require_once 'include/head.php';
  require_once 'include/nav.php';

  $bdd = mysqlConnect();
    try
    {
      $users = $bdd->prepare('SELECT * FROM users ORDER BY nom, prenom');
      $users->execute();
    }
    catch (PDOException $error) {
      die ('Erreur : ' .$error->getMessage());
    }
?>
<!-- body Section-->
<div class="container-fluid">
	<div class="row">
    <div class="col-md-12" style="margin-top:50px">
      <div class="icon"><i class="fa fa-user-o" aria-hidden="true"></i><strong class="text-uppercase"> liste des utilisateurs</strong></div><hr>  
	    <div class="table-responsive">
      	<table id="mytable" class="table table-bordred table-striped">
       		<thead>
         		<th><input type="checkbox" id="checkall"/></th>
         		<th>ID</th>
              <th>Nom</th>
              <th>Prenom</th>
              <th>Utilisateur</th>
              <th>Email</th>
              <th>Avatar</th>
              <th>Rôle</th>
              <th>Sexe</th>
             	<th>Vérifié</th>
              <th></th> 
              <th><a href="create.php"><button class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-trash text-uppercase">Ajouter</span></button></a></th>
          </thead>
			    <tbody>
          <?php foreach ($users as $key => $data) : ?>
					<tr>
						<td><input type="checkbox" class="checkthis"/></td>
						<td><?= $data['id'] ?></td>
						<td><?= strtoupper($data['nom']) ?></td>
						<td><?= ucfirst($data['prenom']) ?></td>
				    <td><?= ucfirst($data['username']) ?></td>
				    <td><?= $data['email'] ?></td>
				    <td><img style="width:35%;" src="asset/img/<?= $data['avatar']; ?>" alt="<?= $data['avatar']; ?>" class="img-fluid rounded-circle"></td>
            <?php if ($data['role'] == 1) : ?>
              <td><img style="width:20%;margin-left:25px;" src="asset/img/admin-user.jpg" class="img-fluid rounded-circle"><br>Administrateur</td>
            <?php else: ?>
              <td>Visiteur</td>
            <?php endif; ?>
            <?php if ($data['sexe'] == 1) : ?>
				      <td><img style="width:35%;" src="asset/img/female-user.jpg" class="img-fluid rounded-circle"></td>
            <?php else: ?>
              <td><img style="width:35%;" src="asset/img/male-user.jpg" class="img-fluid rounded-circle"></td>
            <?php endif; ?>
            <?php if ($data['verified'] == 1) : ?>
              <td><img style="width:35%;" src="asset/img/valide-user.png" class="img-fluid rounded-circle"></td>
            <?php else: ?>
              <td><img style="width:35%;" src="asset/img/unvalide-user.png" class="img-fluid rounded-circle"></td>
            <?php endif; ?>
            <td><a href="user.php?id=<?= $data['id']; ?>"><button class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil text-uppercase">Editer</span></button></p></td>
            <td><a href="delete.php?id=<?= $data['id']; ?>"><button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-pencil text-uppercase">Supprimer</span></button></p></td>
				    <!--<td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil text-uppercase">Editer</span></button></p></td>
				    <td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash text-uppercase">Supprimer</span></button></p></td>-->
			    </tr>
          <?php endforeach; ?>
				 </tbody>
			  </table>
      </div>
	  </div>
  </div>
</div>

<!--<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        		<h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
      		</div>
        	<div class="modal-body">
       			<div class="alert alert-danger"><a href=""><span class="glyphicon glyphicon-warning-sign"></span></a> Are you sure you want to delete this Record?</div>
		 	    </div>
        	<div class="modal-footer ">
		        <button type="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
		        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
      		</div>
        </div>
  	</div>
</div>-->
<?php require_once 'include/footer.php'; ?>