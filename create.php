<?php
  require_once 'include/db.php';
  require_once 'include/head.php';
  require_once 'include/nav.php';

  if (isset($_POST['btn_confirm']))
  {
    $bdd = mysqlConnect();
    try
    {
      $email = $bdd->prepare('SELECT * FROM users WHERE email = ?');
      $email->execute([$_POST['email']]);

      $errors = array();

      if (empty($_POST['username']) || !preg_match('/^[a-zA-Z0-9_]+$/', $_POST['username'])) {
        $errors['username'] = "Pseudo invalide, seul des caractères alphanumérique sont acceptés.";
      } else {
        $user = $bdd->prepare('SELECT * FROM users WHERE username = ?');
        $user->execute([$_POST['username']]);
        $username = $user->fetch();
        if ($username) {
          $errors['username'] = "Ce nom d'utilisateur exite déjà.";
        }
      }

      if(empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $errors['email'] = "l'adresse email n'est pas valide";
      } else {
        $email = $bdd->prepare('SELECT * FROM users WHERE email = ?');
        $email->execute([$_POST['email']]);
        $mail = $email->fetch();
        if ($mail) {
          $errors['username'] = "Cette adresse email exite déjà.";
        }
      }

      if (empty($_POST['password']) || $_POST['password'] != $_POST['passwordconfirm']) {
        $errors['password'] = "Vous devez entrez un mot de passe valide";
      }

      if (isset($_FILES['avatar']))
      {
        if(!in_array(strrchr($_FILES['avatar']['name'], '.'), array('.png', '.gif', '.jpg', '.jpeg')))
          $errors['avatar'] = 'Vous devez choisir un avatar au format png, gif, jpg, jpeg...';
        if(filesize($_FILES['avatar']['tmp_name']) > 2000000000000)
          $errors['avatar'] = 'Le fichier est trop gros...';

        if(empty($errors))
        {
          $avatar = strtr(basename($_FILES['avatar']['name']), 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
          $avatar = preg_replace('/([^.a-z0-9]+)/i', '-', $avatar);
          if(!move_uploaded_file($_FILES['avatar']['tmp_name'], 'asset/img/' . $avatar))
            echo 'Echec de l\'upload !';
          else
          {
            $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
            $key = str_rand_token(12);
            $token = str_rand_token(60);

            $sql = $bdd->prepare('INSERT INTO users (nom, prenom, email, username, sha_pass_hash, token_key, validation_token, validation_token_date, avatar, role, sexe) VALUES (?,?,?,?,?,?,?,?,?,?,?)');
            $sql->execute([$_POST['nom'], $_POST['prenom'], $_POST['email'], $_POST['username'], $password, $key ,$token, NULL, $avatar, isset($_POST['role']) ? 1 : 0, $_POST['sexe']]);

            $user_id = $bdd->lastInsertId();

            // envoi email de confirmation
            /*mail($_POST['email'], '[DASHBORD] Confirmation de votre compte', "Cliquez sur le lien suivant pour valider votre compte.\n\n\n\nhttp://localhost/exercice/dashbord/confirm.php?id=$user_id&$token=$token");*/

            $_SESSION['flash']['success'] = 'un email de confirmation a été envoyé afin de valider le compte ou bien cliquez sur <a href="http://localhost/exercice/dashbord/confirm.php?id='.$user_id.'&token='.$token.'">ce lien</a> pour valider le compte.<br>';

            header('Location: create.php');
            exit();
          }
        }
      }
    }
    catch (PDOException $error) {
      die ('Erreur : ' .$error->getMessage());
    }

    $email->closeCursor();
    $user->closeCursor();
  }
?>

<?php if (!empty($errors)) : ?>
<div class="alert alert-danger">
  <h4 style="margin-left:25px;">Vous n'avez pas correctement rempli le formulaire.</h4>
  <ul>
    <?php foreach ($errors as $error) : ?>
      <li style="margin-left:40px;"><?= $error; ?></li>
    <?php endforeach; ?>
  </ul>
</div>
<?php endif; ?>
<!-- Form -->
<section class="statistics section-padding section-no-padding-bottom">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <form class="form-horizontal" method="post" enctype="multipart/form-data">
          <div class="icon"><i class="fa fa-user-plus" aria-hidden="true"></i><strong class="text-uppercase"> Créer un nouvel utilisateur</strong><hr></div>
          <fieldset>
            <!-- Nom-->
            <div class="form-group">
              <div class="col-md-18">
                <div class="input-group">
                  <span class="input-group-addon">Nom</span>
                  <input id="nom" name="nom" class="form-control" placeholder="nom" required="" type="text">
                </div>
                <p class="help-block">Entrer le nom de l'utilisateur</p>
              </div>
            </div>
            <!-- Prenom-->
            <div class="form-group">
              <div class="col-md-18">
                <div class="input-group">
                  <span class="input-group-addon">Prenom</span>
                  <input id="prenom" name="prenom" class="form-control" placeholder="prénom" required="" type="text">
                </div>
                <p class="help-block">Entrer le prénom de l'utilisateur</p>
              </div>
            </div>
            <!-- Email -->
            <div class="form-group">
              <div class="col-md-18">
                <div class="input-group">
                  <span class="input-group-addon">@</span>
                  <input id="email" name="email" class="form-control" placeholder="adresse e-mail" required="" type="email">
                </div>
                <p class="help-block">Entrer une adresse email</p>
              </div>
            </div>
            <!-- Username -->
            <div class="form-group">
              <div class="col-md-18">
                <div class="input-group">
                  <span class="input-group-addon">Utilisateur</span>
                  <input id="username" name="username" class="form-control" placeholder="username" required="" type="text">
                </div>
                <p class="help-block">Entrer un nom d'utilisateur</p>
              </div>
            </div>
            <!-- Password -->
            <div class="form-group">
              <div class="col-md-18">
                <div class="input-group">
                  <span class="input-group-addon">Password</span>
                  <input id="password" name="password" class="form-control" placeholder="password" required="" type="password">
                </div>
                <p class="help-block">Entrer un mot de passe</p>
              </div>
            </div>
            <!-- Confirm Password -->
            <div class="form-group">
              <div class="col-md-18">
                <div class="input-group">
                  <span class="input-group-addon">Password</span>
                  <input id="passwordconfirm" name="passwordconfirm" class="form-control" placeholder="password" required="" type="password">
                </div>
                <p class="help-block">Confirmer le mot de passe</p>
              </div>
            </div>
            <!-- Select Role -->
            <label class="col-md-18 control-label" for="role">Permissions</label>
            <div class="onoffswitch">
              <input type="checkbox" name="role" class="onoffswitch-checkbox" id="myonoffswitch" checked>
              <label class="onoffswitch-label" for="myonoffswitch">
                  <span value="1" class="onoffswitch-inner"></span>
                  <span value="0" class="onoffswitch-switch"></span>
              </label>
            </div>
            <p class="help-block">Sélectionner un rôle pour l'utilisateur</p>
            <!-- Select Sexe -->
            <div class="form-group">
              <label class="col-md-18 control-label" for="sexe">Sexe</label>
              <div class="col-md-18">
                <select id="sexe" name="sexe" class="form-control">
                  <option value="0">Homme</option>
                  <option value="1">Femme</option>
                </select>
              </div>
            </div>
            <!-- Avatar --> 
            <div class="form-group">
              <label class="col-md-18 control-label" for="avatar">Avatar</label>
              <div class="col-md-18">
                <input type="hidden" name="MAX_FILE_SIZE" value="2000000000">
                <input name="avatar" class="input-file" type="file">
              </div>
            </div>
            <!-- Button -->
            <div class="form-group">
              <div class="col-md-18">
                <button id="btn_confirm" name="btn_confirm" class="btn btn-success">Créer l'utilisateur</button>
                <button id="btn_cancel" name="btn_cancel" class="btn btn-danger">Annuler</button>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</section>
<?php require_once 'include/footer.php'; ?>