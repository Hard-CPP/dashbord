<?php
  require_once 'include/db.php';
  require_once 'include/head.php';
  require_once 'include/nav.php';

  $id = (int) $_GET['id'];
  if(empty($id)) {
  	header('Location: users.php');
  	exit();
  }

  if (isset($_POST['btn_confirm']))
  {
    $bdd = mysqlConnect();
    try
    {
  		$sql = $bdd->prepare('DELETE FROM users WHERE id = ?');
  		$sql->execute(array($id));
    }
    catch (PDOException $error) {
      die ('Erreur : ' .$error->getMessage());
    }

  	echo 'l\'Utilisateur a été supprimé.';
  	header('Location: users.php');
  	exit();
  }

  if (isset($_POST['btn_cancel']))
  {
    header('Location: users.php');
    exit();
  }
?>
<section class="statistics section-padding section-no-padding-bottom">
  <div class="container-fluid">
    <form class="form-horizontal" method="post">
      <button id="btn_confirm" name="btn_confirm" class="btn btn-warning">Supprimer l'utilisateur</button>
      <button id="btn_cancel" name="btn_cancel" class="btn btn-danger">Annuler</button>
    </form>
  </div>
</section>
<?php require_once 'include/footer.php'; ?>