$(document).ready(function($) { 
  $.fn.notif = function(options) {
    var settings = {
      html : '<div class="notification {{cls}} animated fadeInRight">\
      <div class="left">\
        <div class="icon">\
          <i class="{{{icon}}}"></i>\
        </div>\
      </div>\
      <div class="right">\
        <h2>{{{title}}}</h2>\
        <p>{{{content}}}</p>\
      </div>\
    </div>',
    icon: 'fa fa-info',
    timeout: 3000
    }
    if(options.cls == 'error'){
      settings.icon = 'fa fa-exclamation-triangle';
    }
    if(options.cls == 'success'){
      settings.icon = 'fa fa-check';
    }
    var options = $.extend(settings, options);

    return this.each(function(){
      var $this = $(this);
      var $notifs = $('>.notifications', this);
      var $notif = $(Mustache.render(options.html, options));
      if ($notifs.length == 0){
        $notifs = $('<div class="notifications animated flipInX"/>');
        $this.prepend($notifs);
      }
      $notifs.append($notif);
      if(options.timeout){
        setTimeout(function(){
          $notif.trigger('click');
        }, options.timeout)
      }
      $notif.click(function(event){
        event.preventDefault();
        $notif.addClass('fadeOutRight').delay(300).slideUp(400, function(){
          if ($notif.sibling().length == 0){
            $notifs.remove();
          }
          $notif.remove();
        });
      })
    })
  }

  $('.notiferror').click(function(event){
    event.preventDefault();
    $('body').notif({title:'Erreur', content:'Ma description', cls: 'error'});
  })

  $('.notifsuccess').click(function(event){
    event.preventDefault();
    $('body').notif({title:'Bravo', content:'Ma description', cls: 'success'});
  })
});